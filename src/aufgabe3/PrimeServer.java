package aufgabe3;

import rm.requestResponse.Component;
import rm.requestResponse.Message;

import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PrimeServer implements RmiPrimeServerInterface {
    private final static int PORT = 1234;
    private final static Logger LOGGER = Logger.getLogger(PrimeServer.class.getName());
    private int port = PORT;

    private static PrimeServer server;

    PrimeServer(int port) {
        if (port > 0) this.port = port;

        setLogLevel(Level.FINER);
    }

    @Override
    public boolean primeService(long number) {
        for (long i = 2; i < Math.sqrt(number) + 1; i++) {
            if (number % i == 0) return false;
        }
        return true;
    }

    void setLogLevel(Level level) {
        for (Handler h : LOGGER.getLogger("").getHandlers()) h.setLevel(level);
        LOGGER.setLevel(level);
        LOGGER.info("Log level set to " + level);
    }


    public void start() {
        LOGGER.fine("Server started on port " + port);

        try {
            RmiPrimeServerInterface serverStub = (RmiPrimeServerInterface) UnicastRemoteObject.exportObject(server, 0);
            Registry registry = LocateRegistry.createRegistry(port);
            registry.rebind("PrimeServer", serverStub);
            LOGGER.fine("Server is Ready");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        int port = 0;

        for (int i = 0; i < args.length; i++) {
            switch (args[i]) {
                case "-port":
                    try {
                        port = Integer.parseInt(args[++i]);
                    } catch (NumberFormatException e) {
                        LOGGER.severe("port must be an integer, not " + args[i]);
                        System.exit(1);
                    }
                    break;
                default:
                    LOGGER.warning("Wrong parameter passed ... '" + args[i] + "'");
            }
        }
        Scanner scanner = new Scanner(System.in);
        System.out.println("PORT[1234]>");
        port = scanner.nextInt();
        (server = new PrimeServer(port)).start();
    }
}
