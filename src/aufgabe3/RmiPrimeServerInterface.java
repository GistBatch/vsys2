package aufgabe3;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RmiPrimeServerInterface extends Remote {
    boolean primeService(long value) throws RemoteException;
}
