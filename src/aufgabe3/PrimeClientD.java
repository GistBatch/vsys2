package aufgabe3;

import rm.requestResponse.Component;
import rm.requestResponse.Message;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import static java.lang.Thread.interrupted;
import static java.lang.Thread.sleep;

public class PrimeClientD {
    private static final String HOSTNAME = "localhost";
    private static final int PORT = 1234;
    private static final long INITIAL_VALUE = (long) 1e17;
    private static final long COUNT = 20;
    private static final String CLIENT_NAME = PrimeClientD.class.getName();
    private static final int REQUEST_MODE = 0; //0 ist normal, >0 ist mit polling, <0 ist nebenläufig

    private Component communication;
    String hostname;
    int port, requestMode;
    long initialValue, count;

    public PrimeClientD(String hostname, int port, long initialValue, long count, int requestMode) {
        this.hostname = hostname;
        this.port = port;
        this.initialValue = initialValue;
        this.count = count;
        this.requestMode = requestMode;
    }

    public void run() throws RemoteException {

        Registry registry = LocateRegistry.getRegistry(hostname, port);
        System.out.println("Registry bound on " + hostname + ":" + port);
        RmiPrimeServerInterface server = null;
        try {
            server = (RmiPrimeServerInterface) registry.lookup("PrimeServer");
        } catch (NotBoundException e) {
            e.printStackTrace();
        }
        if (requestMode < 0) {
            processNumberConcurrent(initialValue, count, server);
        } else if (requestMode > 0) {
            for (long i = initialValue; i < initialValue + count; i++) processNumberNonBlocking(i, server);
        } else {
            for (long i = initialValue; i < initialValue + count; i++) processNumber(i, server);
        }

    }

    public void processNumberConcurrent(long value, long count, RmiPrimeServerInterface server) {

        for (long i = value; i < value + count; i++) {

            CommunicationThread communicationThread = new CommunicationThread(server, i);
            communicationThread.start();
        }
    }

    public void processNumberNonBlocking(long value, RmiPrimeServerInterface server) throws RemoteException {
        boolean isPrime;
        System.out.print(value + ": ");
        Thread thread = new Thread(() -> {
            while (!interrupted()) {
                System.out.print(".");
                try {
                    sleep(200);
                } catch (InterruptedException e) {

                    break;
                }
            }
        });
        thread.start();
        isPrime = server.primeService(value);
        thread.interrupt();

        System.out.println(isPrime ? "prime" : "not prime");

    }

    public void processNumber(long value, RmiPrimeServerInterface server) throws RemoteException {
        boolean isPrime = server.primeService(value);
        System.out.println(value + ": " + (isPrime ? "prime" : "not prime"));
    }

    public static void main(String args[]) throws IOException {
        String hostname = HOSTNAME;
        int port = PORT, requestMode = REQUEST_MODE;
        long initialValue = INITIAL_VALUE;
        long count = COUNT;

        boolean doExit = false;

        String input;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Welcome to " + CLIENT_NAME + "\n");

        while (!doExit) {
            System.out.print("Server hostname [" + hostname + "] > ");
            input = reader.readLine();
            if (!input.equals("")) hostname = input;

            System.out.print("Server port [" + port + "] > ");
            input = reader.readLine();
            if (!input.equals("")) port = Integer.parseInt(input);

            System.out.print("Request mode [" + getSynchronizedString(requestMode) + "] > ");
            input = reader.readLine();
            if (!input.equals("")) requestMode = Integer.parseInt(input);

            System.out.print("Prime search initial value [" + initialValue + "] > ");
            input = reader.readLine();
            if (!input.equals("")) initialValue = Long.parseLong(input);

            System.out.print("Prime search count [" + count + "] > ");
            input = reader.readLine();
            if (!input.equals("")) count = Integer.parseInt(input);

            new PrimeClientD(hostname, port, initialValue, count, requestMode).run();

            System.out.println("Exit [n]> ");
            input = reader.readLine();
            if (input.equals("y") || input.equals("j")) doExit = true;
        }
    }

    private static String getSynchronizedString(int requestMode) {
        if (requestMode > 0) {
            return "POLLING";
        } else if (requestMode < 0) {
            return "NEBENLÄUFIG";
        } else {
            return "NORMAL";
        }
    }
}
