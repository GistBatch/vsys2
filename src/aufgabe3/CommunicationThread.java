package aufgabe3;

import java.rmi.RemoteException;

public class CommunicationThread extends Thread {

	private RmiPrimeServerInterface server;
	private long value;


	public CommunicationThread(RmiPrimeServerInterface server, long value) {
		this.server= server;
		this.value = value;
	}


	@Override
	public void run() {
		synchronized (CommunicationThread.class) {
			try {
				boolean isPrime = server.primeService(value);
				System.out.println(value + ": " + (isPrime ? "Prime" : "not Prime"));
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
	}


}
